Synchronize between servers and locals run with vagrant.
Easily control vagrant / server configurations with .env.

Control packages by folders.
Install programs and share the configurations with simple scripts.

To be started, first setup a server by followings:

**If you are setting up local server using vagrant & VirtualBox:**

	> Install vagrant
	> Install vb guest additions plugin
		# vagrant plugin install vagrant-vbguest
	> Git clone this
	> Change Vagrantfile.json.example to Vagrantfile.json and configure : You can do this by 'bash init'
	> Start vagrant
		# vagrant up	
	> Log into ssh
		# vagrant ssh	

**If you are setting up actual server:**

	> Log into ssh
	> Git clone this
	
**After you are done with the server setup:**

	> Set symlink ./scripts to /scripts
		# ln -s [src] [to]
	> Change scripts/.env.example to .env and configure
        > Change scripts/.packages.list.example to .packages.list and configure
	> Run desired scripts

**Install and setup programs with simple scripts:**

	// To install all packages listed on .packages.list
	> bash install

	// To install specific package(s) listed on .packages.list
	> bash install $package1 $package2 ..

	// To run the desinated setup & configuration tasks
	> bash setup // for all
	> bash setup $package1 $package2 .. // for specifics

	// To remove
	> bash remove // for all
	> bash remove $package1 $package2 .. // for specifics
